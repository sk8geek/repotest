# Unexpected Behaviour

##Data Repositories 

Persistence to repositories is a PITA when testing. 

In `ControllerSpec`

- In `setup` I create two users, one via the repository and the second 
via a client call to the service
- The first test confirms that the database has two users
- The second test fails as it only find the user persisted via the client 
call to the service

The second user to be created is obviously aware that a user already exists 
as it has an ID of 2. Though Alice ends up with ID 3.
 

