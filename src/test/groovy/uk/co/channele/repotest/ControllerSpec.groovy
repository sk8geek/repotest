package uk.co.channele.repotest

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Shared
import spock.lang.Specification
import uk.co.channele.repotest.domain.User
import uk.co.channele.repotest.repo.UserRepo
import javax.inject.Inject

@MicronautTest
class ControllerSpec extends Specification {

    @Inject
    @Client("/users")
    RxHttpClient client

    @Inject
    UserRepo repo

    @Shared User alice, bob


    void setup() {

        alice = repo.save(new User(username: "alice"))

        def createBob = HttpRequest.POST("/", "{\"username\":\"bob\"}")
        client.toBlocking().exchange(createBob)

    }


    void "check the user count"() {

        expect: "users are persisted"
        repo.count() == 2

    }


    void "can get users via the service"() {

        when: "get list"
        def listFromRepo = repo.findAll()

        then: "there are two users persisted"
        listFromRepo.size() == 2
        listFromRepo.get(0).id == 2
        listFromRepo.get(1).id == 3


        when: "but if we get the list via the application"
        def result = client.toBlocking().retrieve(HttpRequest.GET("/"), Argument.listOf(User.class))

        then: "list should have 2 users"
        result.size() == 2

    }


}
