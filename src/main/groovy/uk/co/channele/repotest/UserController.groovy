package uk.co.channele.repotest

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import uk.co.channele.repotest.domain.User
import uk.co.channele.repotest.service.UserService

import javax.inject.Inject
import javax.validation.constraints.NotNull

@Controller("/users")
class UserController {

    @Inject
    UserService service

    @Get("/")
    def index() {
        def list = service.list()
        return HttpResponse.ok(list)
    }


    @Post("/{?username*}")
    def post(String username) {
        def saved = service.save(new User(username: username))
        if (saved) {
            return HttpResponse.ok(saved)
        }
        return HttpResponse.status(HttpStatus.INTERNAL_SERVER_ERROR)
    }


}
