package uk.co.channele.repotest.repo

import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository
import io.micronaut.data.repository.CrudRepository
import uk.co.channele.repotest.domain.User

@Repository
interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username)

}
