package uk.co.channele.repotest.service

import uk.co.channele.repotest.domain.User
import uk.co.channele.repotest.repo.UserRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserService {

    @Inject
    UserRepo repo


    Iterable<User> list() {
        return repo.findAll() ?: Collections.<User>emptyList()
    }


    User save(User user) {
        if (!user) { return null }
        Optional<User> existing = repo.findByUsername(user.username)
        if (existing.present) {
            return existing.get()
        }
        return repo.save(user)
    }

}
